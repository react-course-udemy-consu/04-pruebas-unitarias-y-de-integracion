import React from 'react';
import ReactDom from 'react-dom';
/* import PrimeraApp from './PrimeraApp'; */
import './index.css';
import CounterApp from './CounterApp';


/* const saludo = <h1>Hola Mundo</h1>; */
const divRoot = document.querySelector('#root');

//Para renderizar la etiqueta h1 dentro del div #root
/* ReactDom.render(saludo, divRoot); */
/* ReactDom.render( <PrimeraApp saludo="Hola mundo"/> , divRoot); */
ReactDom.render( <CounterApp value = { 10 }/> , divRoot);


