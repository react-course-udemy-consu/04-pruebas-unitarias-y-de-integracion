import React from "react";
import '@testing-library/jest-dom'
import { shallow } from "enzyme";
import CounterApp from "../CounterApp";

describe('Pruebas en <CounterApp/>', () => {
  
  let wrapper = shallow( <CounterApp/>);

  beforeEach( () => {

    wrapper = shallow( <CounterApp/>);

  });

	test('Debe de mostrar <CounterApp/> correctamente', () => {

    expect(wrapper).toMatchSnapshot();

	});

	test('Debe de mostrar de mostrar valor por defecto 100', () => {

		const valor = 100;
		const wrapper = shallow( 
			<CounterApp 
				value= { valor }
			/>
		);

		const valorPorDefecto = wrapper.find('h2').text().trim();

		expect(valorPorDefecto).toBe('100');
	});

	test('Debe de incrementar con el botón +1', () => {
    
    wrapper.find('button').at(0).simulate('click');
    const counterText = wrapper.find('h2').text().trim();

    expect(counterText).toBe('11');

	});

  test('Debe de disminuir con el botón -1', () => {
    
    wrapper.find('button').at(2).simulate('click');
    const counterText = wrapper.find('h2').text().trim();

    expect(counterText).toBe('9');

	});

  test('Debe volver al valor inicial con botón Reset', () => {
    

		const wrapper = shallow( 
			<CounterApp 
				value= { 95 }
			/>
		);
    wrapper.find('button').at(0).simulate('click');
    wrapper.find('button').at(1).simulate('click');
    const counterText = wrapper.find('h2').text().trim();
    
    expect(counterText).toBe('95');

	});

});