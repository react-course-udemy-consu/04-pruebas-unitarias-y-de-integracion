import '@testing-library/jest-dom';
import { getSaludo } from "../../base/02-template-string";

describe('Pruebas en el 02-template-string.js', () => {

  test('getSaludo debe retornar Hola Consuelo', () => {
    const nombre = 'Consuelo';
    const saludo = getSaludo( nombre);

    expect( saludo ).toBe( 'Hola ' + nombre);
  })

  test('getSaludo sin argumento debe retornar Hola Carlos', () => {
    const saludo = getSaludo ();

    expect( saludo ).toBe ( 'Hola Carlos' );
  })

});