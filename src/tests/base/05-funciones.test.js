import { getUser, getUsuarioActivo } from '../../base/05-funciones';

describe('Pruebas en 05-funciones', () => {
    test('getUser debe retornar un objeto', () => {
        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }

        const user = getUser();

       expect( user ).toEqual( userTest );
    })

    //Tarea getUsuarioActivo debe de retornar un objeto

    test('getUsuarioActivo debe de retornar un objeto', () => {

        const user = 'Consuelo';
        const userActive = getUsuarioActivo(user);
        const userTest = {
            uid: 'ABC567',
            username: user
        };
        expect( userActive ).toEqual( userTest );
        // También podría ser:
        /* expect( userActive ).toEqual({
            uid: 'ABC567',
            username: user
        }); */
    })
})