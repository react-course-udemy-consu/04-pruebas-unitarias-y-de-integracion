import { getHeroeById, getHeroesByOwner } from '../../base/08-imp-exp';
import heroes from '../../data/heroes';

describe('Pruebas en funciones de Héroes', () => {

  test('debe de retornar un héroe por id', () => {

    const id = 1;
    const heroe = getHeroeById(id);

    const heroeData = heroes.find( h => h.id === id );

    expect(heroe).toEqual(heroeData);
  })
  
  test('debe de retornar un undefine si id Héroe no extiste', () => {

    const id = 10;
    const heroe = getHeroeById(id);

    expect(heroe).toBe(undefined);
  })  
//TAREA
  test('debe de retornar un arreglo con héroes de DC', () => {
    
    const owner = 'DC';
    const heroefilter = getHeroesByOwner(owner);
    const datafilter = heroes.filter(a => a.owner === owner);
    
    console.log(heroefilter);

    expect(heroefilter).toEqual(datafilter);
  })  
//TAREA
  test('debe de retornar un arreglo con héroes de Marvel', () => {
    
    const owner = 'Marvel';
    const marvelLength = 2;
    const datafilter = heroes.filter(a => a.owner === owner);
    
    console.log(datafilter.length);

    expect(marvelLength).toBe(datafilter.length);
  })

})