import React from "react";
import '@testing-library/jest-dom'
import { shallow } from "enzyme";
import PrimeraApp from '../PrimeraApp';

describe('Pruebas en <PrimeraApp />', () => {
  
  /* test('Debe de mostrar el mensaje Hola mundo', () => {
    const saludo = 'Hola mundo';
    const { getByText } = render(<PrimeraApp saludo= { saludo}/>)
    expect( getByText( saludo) ).toBeInTheDocument();
  }); */

  test('Debe de mostrar  <PrimeraApp /> correctamente', () => {

    const saludo = 'Hola mundo'
    const wrapper = shallow( <PrimeraApp saludo= {saludo}/>)

    expect( wrapper ).toMatchSnapshot();
  });

  test('Debe de mostrar  el subtitulo enviado por props', () => {

    const saludo = 'Hola mundo';
    const subTitulo = 'Hola soy un subtitulo';
    const wrapper = shallow( 
      <PrimeraApp 
        saludo= { saludo }
        subtitulo={ subTitulo }
      />
    );

    const textoParrafo = wrapper.find('p').text();

    expect( textoParrafo ).toBe( subTitulo );
    /* expect( wrapper).toMatchSnapshot(); */
   
  });


});
